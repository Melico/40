import React, { Component } from 'react';
//import Map from 'react-amap/lib/map';
import { Map, InfoWindow, Marker, Polyline } from 'react-amap'
import logo from './logo.svg';
import './App.css';

class AppMap extends Component {
  constructor() {
    super();
    this.state = {
      windowvisible: false,
      markervisible: true,
      policemarkervisible:true,
      value: 1,
      position: {
        longitude: 121.370527,
        latitude: 31.171964,
      },
      path:[[{
        longitude: 121.366342,//start
        latitude: 31.170376
      },{
        longitude: 121.365719,//2
        latitude: 31.170167
      },{
        longitude: 121.363166,//3
        latitude: 31.169213,
      },{
        longitude: 121.364448,//4
        latitude: 31.166091,
      },{
        longitude: 121.36529,//5
        latitude: 31.166385,
      },{
        longitude: 121.365505,//6
        latitude: 31.165917,
      },{
        longitude: 121.369694,
        latitude: 31.166895,
      }],[{
        longitude: 121.366342,//start
        latitude: 31.170376
      },{
        longitude: 121.370065,//2
        latitude: 31.171652,
      },{
        longitude: 121.371636,//3
        latitude: 31.172419,
      },{
        longitude: 121.371894,//4 121.372012,31.169816
        latitude: 31.170796,
      },{
        longitude: 121.373171,//5
        latitude: 31.171147,
      },{
        longitude: 121.373568,//6
        latitude: 31.169935,
      },{
        longitude: 121.373589,//7
        latitude: 31.169559,
      },{
        longitude: 121.372173,//8
        latitude: 31.1691,
      },{
        longitude: 121.372613,//9
        latitude: 31.16742,

      },{
        longitude: 121.369694,
        latitude: 31.166895,
      }]],

      size: {
        width: 80,
        height: 50,

      },
      zoom:15,
    }
  }
  render() {
    const plugins = [
      'MapType',
      'Scale',
      'OverView',
      'ControlBar', // v1.1.0 新增
      {
        name: 'ToolBar',
        options: {
          visible: true,  // 不设置该属性默认就是 true
          onCreated(ins){
            console.log(ins);
          },
        },
      }
    ];
    const mykey="b586018704036ad90115b553060cd4d9";
    const styleC = {
      background: `url('http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/map-marker-icon.png')`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      width: '30px',
      height: '40px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '40px'
    };
    const police ={
      background: `url('https://raw.githubusercontent.com/MrTriskin/blog/master/img/hth/police.png')`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      width: '25px',
      height: '25px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '40px'
    };
    const enfant ={
      background: `url('https://raw.githubusercontent.com/MrTriskin/blog/master/img/hth/enfant.png')`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      width: '25px',
      height: '25px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '40px'
    };
    const kepi ={
      background: `url('https://raw.githubusercontent.com/MrTriskin/blog/master/img/hth/kepi.png')`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      width: '25px',
      height: '25px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '40px'
    };
    const moustache ={
      background: `url('https://raw.githubusercontent.com/MrTriskin/blog/master/img/hth/moustache.png')`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      width: '25px',
      height: '25px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '40px'
    };
    const robot ={
      background: `url('https://raw.githubusercontent.com/MrTriskin/blog/master/img/hth/robot.png')`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      width: '25px',
      height: '25px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '40px'
    };
    const food ={
      background: `url('https://raw.githubusercontent.com/MrTriskin/blog/master/img/hth/robot.png')`,
      backgroundSize: 'contain',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      width: '25px',
      height: '25px',
      color: '#000',
      textAlign: 'center',
      lineHeight: '40px'
    };
    return (
      <div className="App">
        {/* <header className="App-header">
          <h1 className="App-title"><br/><br/>Fucking Awesome Map</h1>
        </header> */}
        <div id="app">
        <Map  amapkey={mykey}
              center={{longitude: 121.368488, latitude: 31.170845}}
              plugins={['ToolBar']}
              mapStyle="light"
              zoom={this.state.zoom} >
        <Marker position={{longitude: 121.366342,latitude: 31.170376 }} visible={this.state.markervisible} >
          <div style={styleC}></div>
        </Marker>
        <Marker position={{longitude: 121.371636, latitude: 31.172419 }} visible={this.state.policemarkervisible} >
          <div style={police}></div>
        </Marker>
        <Marker position={{longitude: 121.368983, latitude: 31.172089 }} visible={this.state.policemarkervisible} >
          <div style={enfant}></div>
        </Marker>
        <Marker position={{longitude: 121.371477, latitude: 31.17316 }} visible={this.state.policemarkervisible} >
          <div style={kepi}></div>
        </Marker>

        <Marker position={{longitude: 121.366982, latitude: 31.16631}} visible={this.state.policemarkervisible} >
          <div style={moustache}></div>
        </Marker>
        <Marker position={{longitude: 121.372974, latitude: 31.166815 }} visible={this.state.policemarkervisible} >
          <div style={ robot}></div>
        </Marker>
        <Marker position={{longitude: 121.373338, latitude: 31.169674 }} visible={this.props.foodvisible} >
          <div style={ food}></div>
        </Marker>
        <InfoWindow
            position={this.state.position}
            visible={this.props.windowvisible}
            content={"一个有趣的音乐喷泉。"}
            size={this.state.size}
            events={this.windowEvents}
          />
          <Polyline
            path={ this.state.path[this.props.path] }
            visible={ this.props.linevisible }

          />
        </Map>
        </div>
      </div>
    );
  }
}

export default AppMap;
