import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import TimeLine from 'material-ui/svg-icons/action/timeline';
// import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Slider from 'material-ui/Slider';

import Face from 'material-ui/svg-icons/action/face';
import Favorite from 'material-ui/svg-icons/action/favorite';
import Store from 'material-ui/svg-icons/action/store';
import Transportation from 'material-ui/svg-icons/action/dashboard';
import AppMap from './appmap';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';


import TripDialog from './dialog';
var step_couter = 0;
const styles = {
  title: {
    cursor: 'pointer',
  },
};
const style = {
  margin: 4,
};
/**
 * This example uses an [IconButton](/#/components/icon-button) on the left, has a clickable `title`
 * through the `onClick` property, and a [FlatButton](/#/components/flat-button) on the right.
 */
 export default class Mood extends React.Component {
   state = {
     open:false,
     open1:true,
   }
   handleToggle = () =>{
     this.setState({open: !this.state.open})
   }
   handleClose = () => this.setState({open: false});
   handleClose1 = () => {
     this.setState({open1: false});
   };
   render(){
     return(
       <div style={{height:"100%"}}>
         <AppBar
           title={<span style={styles.title}>TagYourTrip</span>}
           onLeftIconButtonTouchTap={this.handleToggle}
           iconElementLeft={<IconButton><TimeLine /></IconButton>}
           iconElementRight={<TripDialog />}
         />
         <Drawer
           open={this.state.open}
           docked={false}
           onRequestChange={(open) => this.setState({open})}
           >
          <Menu>
           <MenuItem primaryText = "Mood" leftIcon={<Face />} ></MenuItem>
           <MenuItem primaryText = "Preference" leftIcon={<Favorite />} ></MenuItem>
           <MenuItem primaryText = "PassingBy" leftIcon={<Store />} ></MenuItem>
           <MenuItem primaryText = "Transportation" leftIcon={<Transportation />} ></MenuItem>
         </Menu>
         </Drawer>
         <AppMap path = {1}  windowvisible={true}/>
         <Dialog
           // title="Dialog With Date Picker"
           open={this.state.open1}
           onRequestClose={this.handleClose1}
         >
           COME ON! TAG THE WAY YOU LIKE!
           <div>
          <RaisedButton label="Normal" style={style} onClick={this.handleClose1}/>
          <RaisedButton label="Happy" primary={true} style={style} onClick={this.handleClose1}/>
          <RaisedButton label="Angry" secondary={true} style={style} onClick={this.handleClose1}/>
          <RaisedButton label="Nosense" style={style} onClick={this.handleClose1}/>

        </div>

         </Dialog>

       </div>

     );
   }
 }
