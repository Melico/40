import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import TimeLine from 'material-ui/svg-icons/action/timeline';
// import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Slider from 'material-ui/Slider';

import Face from 'material-ui/svg-icons/action/face';
import Favorite from 'material-ui/svg-icons/action/favorite';
import Store from 'material-ui/svg-icons/action/store';
import Transportation from 'material-ui/svg-icons/action/dashboard';
import AppMap from './appmap';


import TripDialog from './dialog';
import TripDialog1 from './dialog0';
var step_couter = 0;
const styles = {
  title: {
    cursor: 'pointer',
  },
};

/**
 * This example uses an [IconButton](/#/components/icon-button) on the left, has a clickable `title`
 * through the `onClick` property, and a [FlatButton](/#/components/flat-button) on the right.
 */
 export default class Pathlast extends React.Component {
   state = {
     open:false,
   }
   handleToggle = () =>{
     this.setState({open: !this.state.open})
   }
   handleClose = () => this.setState({open: false});
   handleClick = () => {this.props.history.push("/mood");}
   render(){
     return(
       <div style={{height:"100%"}}>
         <AppBar
           title={<span style={styles.title}>TagYourTrip</span>}
           onLeftIconButtonTouchTap={this.handleToggle}
           iconElementLeft={<IconButton><TimeLine /></IconButton>}
           iconElementRight={<TripDialog />}
         />
         <Drawer
           open={this.state.open}
           docked={false}
           onRequestChange={(open) => this.setState({open})}
           >
          <Menu>
           <MenuItem primaryText = "Mood" leftIcon={<Face />} onClick={this.handleClick}></MenuItem>
           <MenuItem primaryText = "Preference" leftIcon={<Favorite />} ></MenuItem>
           <MenuItem primaryText = "PassingBy" leftIcon={<Store />} ></MenuItem>
           <MenuItem primaryText = "Transportation" leftIcon={<Transportation />} ></MenuItem>
         </Menu>
         </Drawer>

         <AppMap path = {1} linevisible = {true} foodvisible = {true} windowvisible={true}/>
       </div>

     );
   }
 }
