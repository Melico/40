import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


export default class TripDialog extends React.Component {
  state = {
    open: false,
    value: null,
    toggle: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };
   handleChange = (event, index, value) => this.setState({value});
   handleToggle = () => {
     this.setState({toggle: !this.state.toggle});
     // window.location = "/ini";
    };
  render() {
    const actions = [
      <FlatButton
        label="GO!"
        primary={true}
        href="/ini"
        // keyboardFocused={true}
        onClick={this.handleClose}
      />,
    ];

    return (
      <div>
        <FlatButton label="Let's Go" onClick={this.handleOpen} style = {{marginTop:5, color:"#FFFFFF"}}/>
        <Dialog
          // title="Dialog With Date Picker"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          COME ON! TAG THE WAY YOU LIKE!
          <TextField
            // floatingLabelFixed={true}
            // style={{width:"30%",padding:"5%"}}
            hintText="Here"
            floatingLabelText="From"
            defaultValue="我的位置"
          />
          <TextField
            // floatingLabelFixed={true}
            // style={{width:"30%",padding:"5%"}}
            defaultValue="金虹桥·景亭苑"
            hintText="SW ELSE"
            floatingLabelText="to"
          />
          <SelectField
            floatingLabelText="Purpose"
            value={this.state.value}
            onChange={this.handleChange}
          >
            <MenuItem value={1} primaryText="FastPass" />
            <MenuItem value={2} primaryText="旅游观光" />
            <MenuItem value={3} primaryText="散步" />
            <MenuItem value={4} primaryText="逛街" />
          </SelectField>
        </Dialog>
      </div>
    );
  }
}
