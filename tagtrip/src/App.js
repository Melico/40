import React, { Component } from 'react';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './App.css';

import  HomePage from './start';
import PathIni from './pathIni';
import Pathlast from './pathlast';
import Mood from './pathlast0';
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <MuiThemeProvider>
            <Route exact path="/" component = {HomePage}/>
            <Route exact path="/ini" component = {PathIni}/>
            <Route exact path="/ini/l" component = {Pathlast}/>
            <Route exact path="/mood" component = {Mood}/>
          </MuiThemeProvider>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
