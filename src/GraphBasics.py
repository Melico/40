import numpy as np
#Base Tags define by us can improve me
baseTags=["quiet-busy","slow-fast","boring-happy","life-business","strict-free","quiet-sporty","enjoyment","relaxiation","transport","economy","landmark","weather"]

class Region:
    City = ""
    facility_data =[]
    edges =[]
    def setEdgeProperty(self):
        self.edges = []

class Node:
     def __init__(self,posX,posY):
         self.position_x = posX
         self.position_y = posY

class Edge:
    def __init__(self,pos1_x,pos1_y,pos2_x,pos2_y):
        self.node1 = Node(pos1_x,pos1_y)
        self.node2 = Node(pos2_x,pos2_y)
        self.attribute = np.zeros((len(baseTags),1))

class TempEdge:
    def __init__(self,edge,params):
        self.edge = edge
        self.weight = np.dot(edge.attribute.T,params)

class Path:
    def __init__(self):
        self.edges =[]
        self.XY_route = []
    def getRoute(self):
        self.XY_route =[]
