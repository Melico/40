import numpy as np
from .GraphBasics import *
from .User import *
import  networkx as nx
class Processor:
    styleSize = 21                              # including starting point and destination
    def __init__(self, tags, edges, nodes):
        self.tags = tags
        self.edges = edges
        self.nodes = nodes

    def handleFeedBack(self, path, score):      # HandFeedback(Path p, Score s)
        if score >= 5:
            path.edge.attribute = np.dot(path.edge.attribute,1.2)
        elif (score>=4) and (score<5):
            path.edge.attribute = np.dot(path.edge.attribute,1)
        elif (score>=3) and (score<4):
            path.edge.attribute = np.dot(path.edge.attribute,0.8)
        elif (score>=2) and (score<3):
            path.edge.attribute = np.dot(path.edge.attribute,0.7)
        else:
            path.edge.attribute = np.dot(path.edge.attribute,0.5)

    def testInvalid(self, weightArrary):
        for i in range(8):
            if (weightArrary[i]<0):
                weightArrary[i] = 0
            elif (weightArrary[i]>10):
                weightArrary[i] = 10


    def generalizeInput(self, Input):           # GeneralizeInput(vec input)
        # first 2 element of the Input vector are starting point, destination and purpose
        Weight = np.array([5,5,5,5,5,5,5,5,3,1,0,0])
        for i in range(3, self.styleSize):
            if Input[i]==1:
                if i==3:
                    Weight[2]+=2
                elif i==4:
                    Weight[0]+=2
                    Weight[2]+=2
                elif i==5:
                    Weight[0]-=2
                    Weight[1]-=2
                    Weight[2]-=2
                    Weight[5]-=2
                    Weight[7]+=2
                    Weight[8]-=2
                elif i==6:
                    Weight[2]+=2
                elif i==7:
                    Weight[2]+=2
                elif i==8:
                    Weight[4]+=2
                elif i==9:
                    Weight[0]-=2
                    Weight[7]+=2
                elif i==11:
                    Weight[4]+=2
                elif i==12:
                    Weight[2]+=2
                    Weight[6]+=2
                elif i==13:
                    Weight[1]-=2
                elif i==14:
                    Weight[1]+=2
                elif i==15:
                    Weight[7]+=2
                elif i==16:
                    Weight[8]=2
                elif i==17:
                    Weight[8]=1
                elif i==18:
                    Weight[8]=5
                elif i==19:
                    Weight[10]=1
                elif i==20:
                    Weight[10]=2
            self.testInvalid(Weight)
        return np.transpose(Weight)

    def normalizeLRUData(self, LRU_DATA):                           # NormalizeLRUData(Dictionary<> LRU_DATA)
        max_freq = 0
        v_past = np.array((len(self.tags), 1))
        for i in range(0, len(self.tags)):
            if (LRU_DATA.has_key(self.tags[i])):
                v_past[i] = (LRU_DATA[self.tags[i]])
                if (v_past[i] > max_freq):
                    max_freq = v_past[i]
            else:
                v_past[i] = 0
        normlizedLRU = np.divide(v_past, max_freq)
        return normlizedLRU

    def gotParams(self, userinput, user):                           # GotParams(vec userinput,User user)
        return np.multiply((1 - user.alpha),                           self.generalizeInput(userinput))+                 np.multiply(user.alpha,self.normalizeLRUData(user.LRU_TagData))

    def getEdgeWeights(self,params):                                # GetEdgeWeights(vec params) return mx1 vec of Edge ,where m is the number of edges
        Temp_edges = []
        for i in range(0,len(self.edges)):
            Temp_edges[i] = TempEdge(self.edges[i],np.dot(np.transpose(self.edges[i].attribute),params))
        return  Temp_edges

    def designPath(self, weightedEdges, startX,startY, endX,endY):  # DesignPath(vec weightedEdges) return a path
        # Design a 1 to 1 path  using Dijkstra algorithm
        G =nx.Graph()
        path = Path()
        startNode = None
        endNode = None
        for n in self.nodes:
            if(n.position_x==startX and n.position_y==startY):
                startNode = n
            if(n.position_x==endX and n.position_y==endY):
                endNode = n
        # need to find startnode,endnode
        for temp in weightedEdges.values():
            G.add_edge(temp.edge.node1,temp.edge.node2,weight=temp.weight)

        p = nx.dijkstra_path(G,startNode,endNode,weight='weight')
        for i in range(len(p)-1):
            node1 = p[i]
            node2 =p[i+1]
            if(weightedEdges.get((node1,node2))==None):
                tmp = node1
                node1 =node2
                node2 =tmp
            path.edges.append(weightedEdges[(node1,node2)].edge)
            path.XY_route.append((p[i].position_x, p[i].position_y))
        path.XY_route.append((p[len(p)-1].position_x, p[len(p)-1].position_y))
        return path

    def fetchPathFromSever(self, input):  # FetchPathFromSever(vec input) return 2-3 paths
        # Just link to the server and database then it may work well
        pass
