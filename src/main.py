from .Processor import *
from .GraphBasics import  *
from .User import *
datafile =""

if(__name__ == "__main__"):
    edges=[]
    nodes={}
    # read in datas
    data = open(datafile,"r")
    for line in data.readline():
        edge_data =line.split()
        if(len(edge_data)==4):
            node1= nodes.get((float(edge_data[0]), float(edge_data[1])))
            node2 = nodes.get((float(edge_data[2]), float(edge_data[3])))
            if(node1==None):
                nodes[(float(edge_data[0]), float(edge_data[1]))] = Node(float(edge_data[0]), float(edge_data[1]))
            if(node2==None):
                nodes[(float(edge_data[2]), float(edge_data[3]))] = Node(float(edge_data[2]), float(edge_data[3]))
            edges.append(Edge(nodes[(float(edge_data[0]), float(edge_data[1]))], nodes[(float(edge_data[2]), float(edge_data[3]))]))
    data.close()
    # data processing complete

    #pretend having an input from an user
    input0=np.array((8,1))
    user = User(0,0.3,"sport")
    processor = Processor(baseTags,edges,nodes)
    # step1 , fetch input and generalize it
    # generailized_inputVec = processor.generalizeInput(input)
    # step2 , take use of user's LRU data v_past
    # and got  true_params = generalized_inputVec + alpha* f(v_past)
    # Now do it in one step
    params = processor.gotParams(input0,user)
    # step4 , calculate specific weighted edges for this user
    temp_edges = processor.getEdgeWeights(params)
    # step5, get a recommended path  for this user
    path_recommended1 = processor.designPath(temp_edges,nodes.get(input0[0]),nodes.get(input0[1]))
    # step6, fetch two more recommended route from the server which is generated based on other users' data
    [path_recommended2,path_recommended3]  = processor.fetchPathFromSever(input0)

    feedback = input(prompt ="how do you feel like this plan, rank 0 -> 10")
    # step7, handle feedback and optimal the system
    processor.handleFeedBack(path_recommended1,feedback)
